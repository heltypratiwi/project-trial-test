package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;
import java.security.PublicKey;

/**
 * Created by helty on 4/26/2017.
 */
public class Buku2 extends BaseEntity implements Serializable {
    private String Kode;
    private String Judul;
    private String Isbn;

    public String getkode() { return Kode; }
    public void setkode (String Kode){ this.Kode = Kode;}

    public String getjudul() { return Judul; }
    public void setjudul(String Judul){ this.Judul = Judul;}

    public String getisbn() { return Isbn; }
    public void setisbn(String Isbn){ this.Isbn = Isbn; }

    @Override
    public boolean equals(Object o) {
        return true;
    }
}
