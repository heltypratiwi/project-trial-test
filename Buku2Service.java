package com.greenlabs.trialproject.core.service;

import com.greenlabs.trialproject.core.AppCore;
import com.greenlabs.trialproject.core.common.Constant;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.dao.Buku2DAO;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.Buku2;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * Created by yoggi on 4/26/2017.
 */
public class Buku2Service extends BaseService{

    public Buku2DAO buku2DAO;

    public Result save (final Buku2 buku2) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    buku2.setCreatedBy(AppCore.getInstance().getUserFromSession());
                    if (buku2.getId()==null){
                        buku2DAO.save(buku2);
                    }else {
                        buku2DAO.update(buku2);
                    }
                    }catch (DataAccessException e){
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Result delete(final Buku2 buku2) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    buku2.setkode(buku2.getkode().concat(Constant.FLAG_DELETE));
                    buku2.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    buku2DAO.delete(buku2);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Buku2 findById(Long id) {
        return buku2DAO.findById(id);
    }

    public List<Buku2> find(Buku2 buku2, int offset, int limit) {
        return buku2DAO.find(buku2, offset, limit);
    }

    public int count(Buku2 buku2) {
        return buku2DAO.count(buku2);
    }

}


