/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.greenlabs.trialproject.core.entity.RoleUser;
import com.greenlabs.trialproject.core.entity.User;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
@RemoteServiceRelativePath("springGwtServices/gwtRoleUserService")
public interface GwtRoleUserService extends RemoteService {

    ArrayList<RoleUser> find(User user);
}
