/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.trial.web.client.view.custom;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.web.client.service.GwtBaseService;
import com.greenlabs.trialproject.web.client.view.properties.BaseProperties;
import com.greenlabs.trialproject.core.entity.Role;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.form.ComboBox;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
public abstract class AbstractComboBoxRole implements IsWidget {

    private ListStore<Role> listStore;
    private ComboBox<Role> comboBox;

    @Override
    public Widget asWidget() {
        listStore = new ListStore<>(BaseProperties.getInstance().getRoleProperties().key());
        initData();

        comboBox = new ComboBox<>(listStore, BaseProperties.getInstance().getRoleProperties().labelNama());
        comboBox.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        comboBox.setWidth(120);
        comboBox.setEditable(false);
        comboBox.setForceSelection(true);
        return comboBox;
    }

    private void initData() {
        GwtBaseService.getInstance().getRoleServiceAsync().findAll(new AsyncCallback<ArrayList<Role>>() {
            @Override
            public void onFailure(Throwable caught) {
                AppClient.showMessageOnFailureException(caught);
            }

            @Override
            public void onSuccess(ArrayList<Role> result) {
                listStore.replaceAll(result);
                if (comboBox.getValue() == null) {
                    comboBox.setValue(listStore.get(0));
                }
                loadRelatedMenu();
            }
        });
    }

    public abstract void loadRelatedMenu();
}
