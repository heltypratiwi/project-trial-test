package com.greenlabs.trialproject.core.dao;


import com.greenlabs.trialproject.core.entity.Buku;

/**
 * Created by yoggi on 4/26/2017.
 */
public interface BukuDAO extends BaseDAO<Buku>{
}
