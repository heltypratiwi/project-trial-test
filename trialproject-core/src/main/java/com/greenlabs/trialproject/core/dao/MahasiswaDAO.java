package com.greenlabs.trialproject.core.dao;

import com.greenlabs.trialproject.core.entity.Mahasiswa;

/**
 * Created by Tyas on 11/24/2014.
 */
public interface MahasiswaDAO extends BaseDAO<Mahasiswa> {
}
