/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.dao;


import com.greenlabs.trialproject.core.entity.Role;

/**
 * @author krissadewo
 */
public interface RoleDAO extends BaseDAO<Role> {

}
