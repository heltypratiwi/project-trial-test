package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;
import java.security.PublicKey;

/**
 * Created by helty on 4/26/2017.
 */
public class Buku extends BaseEntity implements Serializable {
    private String Kode;
    private String Judul;
    private String Isbn;

    public String getKode() {
        return Kode;
    }

    public void setKode(String kode) {
        Kode = kode;
    }

    public String getJudul() {
        return Judul;
    }

    public void setJudul(String judul) {
        Judul = judul;
    }

    public String getIsbn() {
        return Isbn;
    }

    public void setIsbn(String isbn) {
        Isbn = isbn;
    }


    @Override
    public String toString() {
        return "Buku{" +
                "Kode='" + Kode + '\'' +
                "Judul='" + Judul + '\'' +
                "Isbn='" + Isbn + '\'' +
                '}';
    }
}
