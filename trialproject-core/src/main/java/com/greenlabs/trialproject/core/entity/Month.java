/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;

/**
 * @author Tejo Baskoro
 */
public class Month implements Serializable {

    private static final long serialVersionUID = -6610163687398266324L;
    private int month;
    private String monthName;

    public Month() {
    }

    public Month(int month, String monthName) {
        this.month = month;
        this.monthName = monthName;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    @Override
    public String toString() {
        return "Month{" +
                "month=" + month +
                ", monthName='" + monthName + '\'' +
                '}';
    }
}
