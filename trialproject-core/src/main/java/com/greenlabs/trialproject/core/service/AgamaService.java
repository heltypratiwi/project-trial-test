package com.greenlabs.trialproject.core.service;

import com.greenlabs.trialproject.core.AppCore;
import com.greenlabs.trialproject.core.common.Constant;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.dao.AgamaDAO;
import com.greenlabs.trialproject.core.entity.Agama;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * Created by kris on 15/05/14.
 */
@Service
public class AgamaService extends BaseService {

    @Autowired
    private AgamaDAO agamaDAO;

    public Result save(final Agama agama) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    agama.setCreatedBy(AppCore.getInstance().getUserFromSession());

                    if (agama.getId() == null) {
                        agamaDAO.save(agama);
                    } else {
                        agamaDAO.update(agama);
                    }
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Result delete(final Agama agama) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    agama.setNama(agama.getNama().concat(Constant.FLAG_DELETE));
                    agama.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    agamaDAO.delete(agama);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Agama findById(Long id) {
        return agamaDAO.findById(id);
    }

    public List<Agama> find(Agama agama, int offset, int limit) {
        return agamaDAO.find(agama, offset, limit);
    }

    public int count(Agama agama) {
        return agamaDAO.count(agama);
    }

}
