/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.service;

import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.dao.RoleMenuDAO;
import com.greenlabs.trialproject.core.entity.RoleMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * @author krissadewo
 */
@Service
public class RoleMenuService extends BaseService {

    @Autowired
    private RoleMenuDAO roleMenuDAO;

    public Result save(final List<RoleMenu> roleMenus) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    for (RoleMenu roleMenu : roleMenus) {
                        if (roleMenu.getId() == null || roleMenu.getId() == 0) {
                            roleMenuDAO.save(roleMenu);
                        } else {
                            roleMenuDAO.update(roleMenu);
                        }
                    }

                } catch (DataAccessException dae) {
                    throw dae;
                }

                return null;
            }
        });

    }
}
