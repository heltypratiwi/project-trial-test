/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.test.dao;

import com.greenlabs.trialproject.core.AppCore;
import com.greenlabs.trialproject.core.dao.UserDAO;
import com.greenlabs.trialproject.core.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author krissadewo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class UserDAOTest {

    @Autowired
    private UserDAO userDAO;
    private User user;

    @Before
    public void init() {
        user = new User();
        user.setId(2l);
    }

    @Test
    @Transactional
    public void delete() {
        AppCore.getLogger(userDAO.delete(user));
    }


}
