package com.greenlabs.trialproject.core.test.service;

import com.greenlabs.trialproject.core.AppCore;
import com.greenlabs.trialproject.core.common.Constant;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.core.service.BukuService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by yoggi on 4/28/2017.
 */
public class BukuServiceTest {

    @Autowired
    private MockHttpSession mockHttpSession;
    @Autowired
    private BukuService bukuService;
    private Buku buku;

    @Before
    public void init() {
        mockHttpSession.setAttribute(Constant.SESSION_USER,new User(1l));

        buku = new Buku();
        buku.setKode("N22");
        buku.setJudul("Hujan");
        buku.setIsbn("978-602-03-2478-4");

        buku.setCreatedBy((User) mockHttpSession.getAttribute(Constant.SESSION_USER));
    }

    @Test
    @Transactional
    public void save() {
        bukuService.save(buku);

    }

    @Test
    @Transactional
    public void delete() {
        System.out.println(bukuService.delete(buku));
    }

    @Test
    @Transactional
    public void count() {
        bukuService.count(buku);
    }

    @Test
    @Transactional
    public void findById() {
        bukuService.findById(buku.getId());
    }

    @Test
    @Transactional
    public void find() {
        bukuService.find(buku, 0, Integer.MAX_VALUE);
    }

}

