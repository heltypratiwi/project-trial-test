/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.test.service;

import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.core.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author krissadewo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class UserServiceTest {

    @Autowired
    private UserService userService;
    private User user;

    @Before
    public void init() {
        user = new User();
        user.setId(2l);
    }

    @Test
    @Transactional
    public void delete() {
        System.out.println(userService.delete(user));
    }


}
