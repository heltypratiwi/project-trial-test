/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.greenlabs.trialproject.web.client.service.GwtBaseService;
import com.greenlabs.trialproject.web.client.view.HomeView;
import com.greenlabs.trialproject.web.client.view.LoginView;
import com.greenlabs.trialproject.web.client.view.View;
import com.sencha.gxt.core.client.GXT;
import com.sencha.gxt.state.client.CookieProvider;
import com.sencha.gxt.state.client.StateManager;

/**
 * @author kris
 */
public class AppModule implements EntryPoint {

    @Override
    public void onModuleLoad() {
        StateManager.get().setProvider(new CookieProvider("/", null, null, GXT.isSecure()));
        checkAuthenticatedUser();
        RootPanel.get().clear();
    }

    private void checkAuthenticatedUser() {
        GwtBaseService.getInstance().getUserServiceAsync().isAuthenticatedUser(new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                AppClient.showMessageOnFailureException(caught);
            }

            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    RootPanel.get().add(View.loadView(new HomeView()));
                } else {
                    RootPanel.get().add(View.loadView(new LoginView()));
                }
            }
        });
    }
}
