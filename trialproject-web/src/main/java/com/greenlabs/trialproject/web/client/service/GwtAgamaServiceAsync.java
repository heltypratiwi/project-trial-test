package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Agama;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

public interface GwtAgamaServiceAsync {

    void save(Agama agama, AsyncCallback<Result> callback);

    void delete(Agama agama, AsyncCallback<Result> callback);

    void find(Agama agama, PagingLoadConfig pagingLoadConfig, AsyncCallback<PagingLoadResult<Agama>> callback);

    void find(Agama agama, AsyncCallback<ArrayList<Agama>> callback);
}
