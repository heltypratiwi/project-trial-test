/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.core.shared.GWT;

/**
 * @author krissadewo
 */
public class GwtBaseService {

    private final GwtUserServiceAsync userServiceAsync = GWT.create(GwtUserService.class);
    private final GwtMenuServiceAsync menuServiceAsync = GWT.create(GwtMenuService.class);
    private final GwtRoleServiceAsync roleServiceAsync = GWT.create(GwtRoleService.class);
    private final GwtRoleMenuServiceAsync roleMenuServiceAsync = GWT.create(GwtRoleMenuService.class);
    private final GwtRoleUserServiceAsync roleUserServiceAsync = GWT.create(GwtRoleUserService.class);
    private final GwtSessionHandlerServiceAsync sessionHandlerServiceAsync = GWT.create(GwtSessionHandlerService.class);
    private final GwtHomeServiceAsync homeServiceAsync = GWT.create(GwtHomeService.class);
    private final GwtAgamaServiceAsync agamaServiceAsync = GWT.create(GwtAgamaService.class);
    private final GwtBukuServiceAsync bukuServiceAsync = GWT.create(GwtBukuService.class);

    private GwtBaseService() {
    }

    public static GwtBaseService getInstance() {
        return BaseServiceHolder.INSTANCE;
    }


    public GwtUserServiceAsync getUserServiceAsync() {
        return userServiceAsync;
    }

    public GwtMenuServiceAsync getMenuServiceAsync() {
        return menuServiceAsync;
    }

    public GwtRoleServiceAsync getRoleServiceAsync() {
        return roleServiceAsync;
    }

    public GwtRoleMenuServiceAsync getRoleMenuServiceAsync() {
        return roleMenuServiceAsync;
    }

    public GwtRoleUserServiceAsync getRoleUserServiceAsync() {
        return roleUserServiceAsync;
    }

    public GwtSessionHandlerServiceAsync getSessionHandlerServiceAsync() {
        return sessionHandlerServiceAsync;
    }

    public GwtHomeServiceAsync getHomeServiceAsync() {
        return homeServiceAsync;
    }

    private static class BaseServiceHolder {
        private static final GwtBaseService INSTANCE = new GwtBaseService();
    }

    public GwtAgamaServiceAsync getAgamaServiceAsync() {
        return agamaServiceAsync;
    }

    public GwtBukuServiceAsync getBukuServiceAsync() {
        return bukuServiceAsync;
    }

}
