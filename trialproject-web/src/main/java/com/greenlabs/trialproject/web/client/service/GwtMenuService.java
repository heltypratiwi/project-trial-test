/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.greenlabs.trialproject.core.entity.Menu;
import com.greenlabs.trialproject.core.entity.Role;

import java.util.ArrayList;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 18, 2013
 */
@RemoteServiceRelativePath("springGwtServices/gwtMenuService")
public interface GwtMenuService extends RemoteService {

    ArrayList<Menu> createTreeMenu();

    ArrayList<Menu> createTreeMenu(Role role);
}
