package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.core.entity.Menu;
import com.greenlabs.trialproject.core.entity.Role;

import java.util.ArrayList;

public interface GwtMenuServiceAsync {

    void createTreeMenu(AsyncCallback<ArrayList<Menu>> callback);

    void createTreeMenu(Role role, AsyncCallback<ArrayList<Menu>> callback);
}
