/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.RoleMenu;

import java.util.List;

/**
 * @author krissadewo
 */
@RemoteServiceRelativePath("springGwtServices/gwtRoleMenuService")
public interface GwtRoleMenuService extends RemoteService {

    Result save(List<RoleMenu> roleMenus);
}
