/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.core.entity.Role;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
public interface GwtRoleServiceAsync {

    void findAll(AsyncCallback<ArrayList<Role>> callback);
}
