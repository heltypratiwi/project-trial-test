/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.User;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * @author kris
 */
@RemoteServiceRelativePath("springGwtServices/gwtUserService")
public interface GwtUserService extends RemoteService {

    Result delete(User user);

    boolean isAuthenticatedUser();

    User doLogin(User user);

    void doLogout();

    User getUserFromSession();

    ArrayList<User> findAll();

    PagingLoadResult<User> find(PagingLoadConfig pagingLoadConfig);

    Result save(User user);
}
