/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.User;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
public interface GwtUserServiceAsync {

    void delete(User user, AsyncCallback<Result> callback);

    void isAuthenticatedUser(AsyncCallback<Boolean> callback);

    void doLogin(User user, AsyncCallback<User> callback);

    void doLogout(AsyncCallback<Void> callback);

    void getUserFromSession(AsyncCallback<User> callback);

    void findAll(AsyncCallback<ArrayList<User>> callback);

    void find(PagingLoadConfig config, AsyncCallback<PagingLoadResult<User>> callback);

    void save(User user, AsyncCallback<Result> callback);
}
