/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Menu;
import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.web.client.icon.Icon;
import com.greenlabs.trialproject.web.client.view.custom.GridView;
import com.greenlabs.trialproject.web.client.view.menu.dashboard.DashboardView;
import com.greenlabs.trialproject.web.client.view.menu.master.AgamaView;
import com.greenlabs.trialproject.web.client.view.menu.master.BukuView;
import com.greenlabs.trialproject.web.client.view.menu.settings.HakAksesView;
import com.greenlabs.trialproject.web.client.view.menu.settings.UserView;
import com.greenlabs.trialproject.web.client.wrapper.CallbackWrapper;
import com.greenlabs.trialproject.web.client.wrapper.MenuWrapper;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.core.client.Style;
import com.sencha.gxt.core.client.resources.ThemeStyles;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.data.shared.Store;
import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.TabItemConfig;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.*;
import com.sencha.gxt.widget.core.client.event.CloseEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.form.StoreFilterField;
import com.sencha.gxt.widget.core.client.menu.Item;
import com.sencha.gxt.widget.core.client.menu.MenuItem;
import com.sencha.gxt.widget.core.client.selection.SelectionChangedEvent;
import com.sencha.gxt.widget.core.client.selection.SelectionChangedEvent.SelectionChangedHandler;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;
import com.sencha.gxt.widget.core.client.tree.Tree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author krissadewo
 */
public final class HomeView extends View {

    private final List<MenuWrapper> currentActiveMenus = new ArrayList<>();
    private List<Menu> menus;
    private LayoutPanel northPanel;
    private ContentPanel westPanel;
    private Tree<Menu, String> treeMenu;
    private TabPanel tabPanel;

    @Override
    public Widget asWidget() {
        final BorderLayoutContainer borderLayoutContainer = new BorderLayoutContainer();

        menus = new ArrayList<>();
        northPanel = new LayoutPanel();
        westPanel = new ContentPanel();

        final Label title = new Label("");
        title.setStyleName("app-title-header");

        final HorizontalLayoutContainer vBoxLayoutContainer = new HorizontalLayoutContainer();
        vBoxLayoutContainer.setStyleName("demo-header");

        com.sencha.gxt.widget.core.client.menu.Menu menu = new com.sencha.gxt.widget.core.client.menu.Menu();
        MenuItem menuItemLogout = new MenuItem("Logout");
        menuItemLogout.setIcon(Icon.INSTANCE.logout());
        menuItemLogout.addSelectionHandler(new SelectionHandler<Item>() {
            @Override
            public void onSelection(SelectionEvent<Item> event) {
                doLogout();
            }
        });
        menu.add(menuItemLogout);
        menu.add(new MenuItem("Info", Icon.INSTANCE.info()));

        final TextButton button = new TextButton("", Icon.INSTANCE.user());
        button.setMenu(menu);
        button.setIconAlign(ButtonCell.IconAlign.BOTTOM);
        button.setMenuAlign(new Style.AnchorAlignment(Style.Anchor.BOTTOM_RIGHT));
        button.setArrowAlign(ButtonCell.ButtonArrowAlign.BOTTOM);
        button.setShadow(true);

        vBoxLayoutContainer.addResizeHandler(new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent event) {
                vBoxLayoutContainer.clear();
                vBoxLayoutContainer.add(title, new HorizontalLayoutContainer.HorizontalLayoutData(getScreenWidth() - 45, 0));
                vBoxLayoutContainer.add(button, new HorizontalLayoutContainer.HorizontalLayoutData());
            }
        });
        vBoxLayoutContainer.setLayoutData(new VerticalLayoutContainer.VerticalLayoutData());

        northPanel.setHeight("150px");
        northPanel.add(vBoxLayoutContainer);

        loadMenu();

        BorderLayoutContainer.BorderLayoutData westData = new BorderLayoutContainer.BorderLayoutData(250);
        westData.setMargins(new Margins(0, 7, 0, 0));
        westData.setCollapsible(true);
        westData.setSplit(true);
        westData.setCollapseMini(true);

        borderLayoutContainer.setHeight("800px");
        borderLayoutContainer.setNorthWidget(northPanel, new BorderLayoutContainer.BorderLayoutData(38));
        borderLayoutContainer.setWestWidget(westPanel, westData);
        borderLayoutContainer.setCenterWidget(createTabPanel(), new MarginData(0));

        getService().getHomeServiceAsync().getAppTitle(new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(String result) {
                title.setText(result);

            }
        });

        Viewport viewport = new Viewport();
        viewport.add(borderLayoutContainer);
        return viewport;
    }

    private void loadMenu() {
        new CallbackWrapper<ArrayList<Menu>>() {

            @Override
            protected void onSuccess(ArrayList<Menu> result) {
                menus = result;
                westPanel.setBorders(false);
                westPanel.setBodyBorder(false);
                westPanel.add(createNavigation());
                westPanel.forceLayout();
                showCurrentMenu(History.getToken());
            }

            @Override
            protected void onFailure(Throwable throwable) {
                AppClient.showMessageOnFailureException(throwable);
            }

            @Override
            protected void onCall(AsyncCallback<ArrayList<Menu>> callback) {
                getService().getMenuServiceAsync().createTreeMenu(callback);
            }
        }.call();
    }

    private Widget createNavigation() {
        getService().getUserServiceAsync().getUserFromSession(new AsyncCallback<User>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(User result) {
                westPanel.setHeadingText(result.getRealname() + "#" + result.getRole().getNama());
                westPanel.getHeader().addStyleName(ThemeStyles.get().borderColor());
            }
        });

        TreeStore<Menu> treeStore = new TreeStore<>(getProperties().getMenuProperties().key());
        treeMenu = new Tree<>(treeStore, getProperties().getMenuProperties().valueNamaMenu());
        treeMenu.getSelectionModel().setSelectionMode(Style.SelectionMode.SINGLE);
        treeMenu.getSelectionModel().addSelectionChangedHandler(treeMenuSelectionHandler());
        treeMenu.getStyle().setLeafIcon(Icon.INSTANCE.menuIcon());

        //Create menu from data
        for (Menu menu : menus) {
            if (menu.getIdParent() == 0) {
                treeStore.add(menu);
            } else {
                processChildMenu(treeStore, menu);
            }
            treeMenu.setExpanded(menu, true);
        }

        StoreFilterField<Menu> filter = new StoreFilterField<Menu>() {

            @Override
            protected boolean doSelect(Store<Menu> store, Menu parent, Menu menu, String filter) {
                return menu.getNama().toLowerCase().contains(filter.toLowerCase());
            }
        };

        filter.setEmptyText("Filter...");
        filter.bind(treeStore);

        ToolBar toolBarFilter = new ToolBar();
        toolBarFilter.add(filter);

        VerticalLayoutContainer verticalLayoutContainer = new VerticalLayoutContainer();
        verticalLayoutContainer.add(toolBarFilter, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        verticalLayoutContainer.add(treeMenu, new VerticalLayoutContainer.VerticalLayoutData(1, 1));

        return verticalLayoutContainer;
    }

    private SelectionChangedHandler<Menu> treeMenuSelectionHandler() {
        return new SelectionChangedHandler<Menu>() {
            @Override
            public void onSelectionChanged(SelectionChangedEvent<Menu> event) {
                if (!event.getSelection().isEmpty()) {
                    Menu menu = event.getSelection().get(0);
                    for (MenuWrapper activeMenu : currentActiveMenus) {
                        if (menu.getTitle().equals(activeMenu.getMenu().getTitle())) {
                            tabPanel.setActiveWidget(activeMenu.getWidget());
                            return;
                        }
                    }

                    setSelectedMenu(menu);
                }
            }
        };
    }

    private void processChildMenu(TreeStore<Menu> store, Menu menu) {
        for (Menu parent : menus) {
            if (parent.getId().equals(menu.getIdParent())) {
                store.add(parent, menu);
            }
        }
    }

    public TabPanel createTabPanel() {
        tabPanel = new TabPanel();
        tabPanel.setBodyBorder(true);
        tabPanel.setBorders(false);
        tabPanel.setTabScroll(true);
        tabPanel.setCloseContextMenu(true);
        tabPanel.addSelectionHandler(tabMenuSelectionHandler());
        tabPanel.addCloseHandler(tabCloseHandler());
        tabPanel.addResizeHandler(new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent event) {
                for (MenuWrapper menuWrapper : currentActiveMenus) {
                    resizeGridView(menuWrapper);
                }
            }
        });
        //Default opened tab menu
        VerticalLayoutContainer container = new VerticalLayoutContainer();
        container.add(new DashboardView().asWidget(), new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        container.sync(true);
        tabPanel.add(container, new TabItemConfig("Dashboard", false));
        tabPanel.setActiveWidget(container);
        return tabPanel;
    }

    private SelectionHandler<Widget> tabMenuSelectionHandler() {
        return new SelectionHandler<Widget>() {
            @Override
            public void onSelection(SelectionEvent<Widget> event) {
                Widget widget = event.getSelectedItem();

                for (MenuWrapper menuWrapper : currentActiveMenus) {
                    if (tabPanel.getConfig(widget).getText().equals(menuWrapper.getMenu().getTitle())) {
                        treeMenu.getSelectionModel().select(menuWrapper.getMenu(), false);
                        resizeGridView(menuWrapper);
                        break;
                    }
                }
            }
        };
    }

    private void doLogout() {
        new CallbackWrapper<Void>() {
            @Override
            protected void onSuccess(Void result) {
                Window.Location.reload();
            }

            @Override
            protected void onFailure(Throwable throwable) {
            }

            @Override
            protected void onCall(AsyncCallback<Void> callback) {
                History.newItem("");
                History.fireCurrentHistoryState();
                getService().getUserServiceAsync().doLogout(callback);
            }
        }.call();
    }

    private CloseEvent.CloseHandler<Widget> tabCloseHandler() {
        return new CloseEvent.CloseHandler<Widget>() {
            @Override
            public void onClose(CloseEvent<Widget> event) {
                /**
                 * We should consider about concurent modification exception
                 * when removing object from array list, so we need using
                 * iterator for this case,
                 */
                Iterator<MenuWrapper> iterator = currentActiveMenus.iterator();
                while (iterator.hasNext()) {
                    MenuWrapper item = iterator.next();
                    if (item.getMenu().getTitle().equalsIgnoreCase(event.getItem().getTitle())) {
                        iterator.remove();
                    }
                }

                if (currentActiveMenus.isEmpty()) {
                    treeMenu.getSelectionModel().deselectAll();
                    treeMenu.getSelectionModel().refresh();
                    History.newItem("");
                } else {
                    History.newItem(currentActiveMenus.get(currentActiveMenus.size() - 1).getMenu().getTitle());
                }

                History.fireCurrentHistoryState();
            }
        };
    }

    private void addTabMenu(final Menu menu, final Widget view) {
        /**
         * Check valid session from request for each form
         */
        new CallbackWrapper<Boolean>() {
            @Override
            protected void onSuccess(Boolean result) {
                if (!result) {
                    AppClient.showInfoMessage(Result.SESSION_EXPIRED).addHideHandler(new HideEvent.HideHandler() {
                        @Override
                        public void onHide(HideEvent event) {
                            getService().getUserServiceAsync().doLogout(new AsyncCallback<Void>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                }

                                @Override
                                public void onSuccess(Void result) {
                                    Window.Location.reload();
                                }
                            });
                        }
                    });
                } else {
                    view.setPixelSize(tabPanel.getElement().getComputedWidth() - 20, tabPanel.getElement().getComputedHeight() - 50);

                    VBoxLayoutContainer vBoxLayoutContainer = new VBoxLayoutContainer();
                    vBoxLayoutContainer.add(view);
                    vBoxLayoutContainer.setTitle(menu.getTitle());
                    vBoxLayoutContainer.setPack(BoxLayoutContainer.BoxLayoutPack.START);
                    vBoxLayoutContainer.setVBoxLayoutAlign(VBoxLayoutContainer.VBoxLayoutAlign.STRETCHMAX);
                    vBoxLayoutContainer.setLayoutData(new BoxLayoutContainer.BoxLayoutData(new Margins(8, 8, 8, 8)));
                    vBoxLayoutContainer.setAdjustForFlexRemainder(true);
                    vBoxLayoutContainer.focus();

                    tabPanel.add(vBoxLayoutContainer, new TabItemConfig(menu.getTitle(), true));
                    tabPanel.setTitle(menu.getTitle());
                    tabPanel.setActiveWidget(vBoxLayoutContainer);
                    currentActiveMenus.add(new MenuWrapper(menu, vBoxLayoutContainer));
                }
            }

            @Override
            protected void onFailure(Throwable throwable) {
            }

            @Override
            protected void onCall(AsyncCallback<Boolean> callback) {
                getService().getSessionHandlerServiceAsync().isValidSession(callback);
            }
        }.call();
    }

    /**
     * After west panel is expand or collapse <p>
     * size of grid need manualy to be adjust to the layout
     *
     * @param menuWrapper menuWrapper
     */
    private void resizeGridView(MenuWrapper menuWrapper) {
        VBoxLayoutContainer container = (VBoxLayoutContainer) menuWrapper.getWidget();
        GridView gridView = (GridView) container.getWidget(0);
        gridView.setPixelSize(tabPanel.getElement().getComputedWidth() - 20, tabPanel.getElement().getComputedHeight() - 50);
        gridView.forceLayout();
    }

    public void showCurrentMenu(String token) {
        if (token != null && !token.isEmpty()) {
            for (Menu menu : menus) {
                if (token.equals(menu.getTitle())) {
                    setSelectedMenu(menu);
                }
            }
        }
    }

    private void setSelectedMenu(Menu menu) {
        History.newItem(menu.getTitle());
        if (menu.getTitle().equals("Logout")) {
            doLogout();
        } else
            //Dont't remove this style as you can
            //######################## SETTING ###############################
            if (menu.getKode().equals(VIEW_SETTING_HAK_AKSES)) {
                addTabMenu(menu, new HakAksesView().asWidget());
            } else if (menu.getKode().equals(VIEW_SETTING_USER_MANAGEMENT)) {
                addTabMenu(menu, new UserView(menu).asWidget());
            } else if (menu.getKode().equals(VIEW_MASTER_AGAMA)) {
                addTabMenu(menu, new AgamaView(menu).asWidget());
            } else if (menu.getKode().equals(VIEW_MASTER_BUKU)) {
                addTabMenu(menu, new BukuView(menu).asWidget());
            }
    }
}
