package com.greenlabs.trialproject.web.client.view;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.web.client.service.GwtBaseService;
import com.greenlabs.trialproject.web.client.view.handler.FormHandler;
import com.greenlabs.trialproject.web.client.view.properties.BaseProperties;
import com.greenlabs.trialproject.core.entity.Menu;

/**
 * @author krissadewo
 *         <p />Base class for user view, providing base method to child class
 */
public abstract class View implements IsWidget {

    //########################## APOTEK ############################
    public static final String VIEW_APOTEK_ANTRIAN_RESEP = "VIEW_APOTEK_ANTRIAN_RESEP";
    public static final String VIEW_APOTEK_ENTRI_RESEP = "VIEW_APOTEK_ENTRI_RESEP";
    public static final String VIEW_APOTEK_PENERIMAAN_OBAT = "VIEW_APOTEK_PENERIMAAN_OBAT";
    public static final String VIEW_APOTEK_PENGELUARAN_OBAT = "VIEW_APOTEK_PENGELUARAN_OBAT";
    //###################### PENDAFTARAN ############################
    public static final String VIEW_PENDAFTARAN_RAWAT_JALAN = "VIEW_PENDAFTARAN_RAWAT_JALAN";
    public static final String VIEW_PENDAFTARAN_RAWAT_INAP = "VIEW_PENDAFTARAN_RAWAT_INAP";
    public static final String VIEW_PENDAFTARAN_UGD = "VIEW_PENDAFTARAN_UGD";
    //###################### PELAYANAN ############################
    public static final String VIEW_PELAYANAN_ANTRIAN = "VIEW_PELAYANAN_ANTRIAN";
    public static final String VIEW_PELAYANAN_OPERASI = "VIEW_PELAYANAN_OPERASI";
    public static final String VIEW_PELAYANAN_PASIEN = "VIEW_PELAYANAN_PASIEN";
    //######################## MASTER ###############################
    public static final String VIEW_MASTER_MOCK = "VIEW_MASTER_MOCK";
    public static final String VIEW_MASTER_AGAMA = "VIEW_MASTER_AGAMA";
    public static final String VIEW_MASTER_BUKU = "VIEW_MASTER_BUKU";
    public static final String VIEW_MASTER_MAHASISWA = "VIEW_MASTER_MAHASISWA";
    public static final String VIEW_MASTER_PEMINJAMAN = "VIEW_MASTER_PEMINJAMAN";
    public static final String VIEW_MASTER_ASURANSI = "VIEW_MASTER_ASURANSI";
    public static final String VIEW_MASTER_BANGSAL = "VIEW_MASTER_BANGSAL";
    public static final String VIEW_MASTER_CARA_BAYAR = "VIEW_MASTER_CARA_BAYAR";
    public static final String VIEW_MASTER_CARA_MASUK = "VIEW_MASTER_CARA_MASUK";
    public static final String VIEW_MASTER_DOKTER = "VIEW_MASTER_DOKTER";
    public static final String VIEW_MASTER_DOKTER_JADWAL = "VIEW_MASTER_DOKTER_JADWAL";
    public static final String VIEW_MASTER_DOKTER_TUGAS = "VIEW_MASTER_DOKTER_TUGAS";
    public static final String VIEW_MASTER_DIAGNOSA = "VIEW_MASTER_DIAGNOSA";
    public static final String VIEW_MASTER_JENIS_VENDOR = "VIEW_MASTER_JENIS_VENDOR";
    public static final String VIEW_MASTER_KABUPATEN = "VIEW_MASTER_KABUPATEN";
    public static final String VIEW_MASTER_KAMAR = "VIEW_MASTER_KAMAR";
    public static final String VIEW_MASTER_KECAMATAN = "VIEW_MASTER_KECAMATAN";
    public static final String VIEW_MASTER_KELAS = "VIEW_MASTER_KELAS";
    public static final String VIEW_MASTER_KELURAHAN = "VIEW_MASTER_KELURAHAN";
    public static final String VIEW_MASTER_OBAT = "VIEW_MASTER_OBAT";
    public static final String VIEW_MASTER_GOLONGAN_OBAT = "VIEW_MASTER_GOLONGAN_OBAT";
    public static final String VIEW_MASTER_JENIS_OBAT = "VIEW_MASTER_JENIS_OBAT";
    public static final String VIEW_MASTER_KEPEMILIKAN_OBAT = "VIEW_MASTER_KEPEMILIKAN_OBAT";
    public static final String VIEW_MASTER_SATUAN_BESAR_OBAT = "VIEW_MASTER_SATUAN_OBAT_BESAR";
    public static final String VIEW_MASTER_SATUAN_KECIL_OBAT = "VIEW_MASTER_SATUAN_OBAT_KECIL";
    public static final String VIEW_MASTER_SEDIAAN_OBAT = "VIEW_MASTER_SEDIAAN_OBAT";
    public static final String VIEW_MASTER_TERAPI_OBAT = "VIEW_MASTER_TERAPI_OBAT";
    public static final String VIEW_MASTER_PASIEN = "VIEW_MASTER_PASIEN";
    public static final String VIEW_MASTER_JENIS_PASIEN = "VIEW_MASTER_JENIS_PASIEN";
    public static final String VIEW_MASTER_PEKERJAAN = "VIEW_MASTER_PEKERJAAN";
    public static final String VIEW_MASTER_PENDIDIKAN = "VIEW_MASTER_PENDIDIKAN";
    public static final String VIEW_MASTER_PROPINSI = "VIEW_MASTER_PROPINSI";
    public static final String VIEW_MASTER_POLIKLINIK = "VIEW_MASTER_POLIKLINIK";
    public static final String VIEW_MASTER_PRODUK = "VIEW_MASTER_PRODUK";
    public static final String VIEW_MASTER_GOLONGAN_PRODUK = "VIEW_MASTER_GOLONGAN_PRODUK";
    public static final String VIEW_MASTER_KOMPONEN_BIAYA = "VIEW_MASTER_KOMPONEN_BIAYA";
    public static final String VIEW_MASTER_KOMPONEN_BIAYA_PRODUK = "VIEW_MASTER_KOMPONEN_BIAYA_PRODUK";
    public static final String VIEW_MASTER_RUJUKAN = "VIEW_MASTER_RUJUKAN";
    public static final String VIEW_MASTER_SPESIALISASI = "VIEW_MASTER_SPESIALISASI";
    public static final String VIEW_MASTER_SUB_SPESIALISASI = "VIEW_MASTER_SUB_SPESIALISASI";
    public static final String VIEW_MASTER_UNIT = "VIEW_MASTER_UNIT";
    public static final String VIEW_MASTER_VENDOR = "VIEW_MASTER_VENDOR";
    //######################## SETTING ###############################
    public static final String VIEW_SETTING_HARGA_PRODUK = "VIEW_SETTING_HARGA_PRODUK";
    public static final String VIEW_SETTING_HARGA_OBAT = "VIEW_SETTING_HARGA_OBAT";
    public static final String VIEW_SETTING_HAK_AKSES = "VIEW_SETTING_HAK_AKSES";
    public static final String VIEW_SETTING_USER_MANAGEMENT = "VIEW_SETTING_USER_MANAGEMENT";
    private Menu menu;


    public View() {
    }

    public View(Menu menu) {
        this.menu = menu;
    }

    /**
     * Call this method to load specific view
     *
     * @param view view
     * @return specific view
     */
    public static Widget loadView(View view) {
        return view.asWidget();
    }

    public static native int getScreenWidth() /*-{
     return $wnd.screen.width;
     }-*/;

    public static native int getScreenHeight() /*-{
     return $wnd.screen.height;
     }-*/;

    @Override
    public Widget asWidget() {
        return asWidget();
    }

    /**
     * All about RPC service will be use on client application
     *
     * @return service
     */
    protected GwtBaseService getService() {
        return GwtBaseService.getInstance();
    }

    protected BaseProperties getProperties() {
        return BaseProperties.getInstance();
    }

    /**
     * Override this method with specific name of view. View name will be used
     * to selection for what view will be used when tree menu has changed
     *
     * @return
     */
    public Menu getMenu() {
        return menu;
    }

    public void registerShowEvent(final FormHandler formHandler, final View view) {
        Event.addNativePreviewHandler(new Event.NativePreviewHandler() {
            @Override
            public void onPreviewNativeEvent(Event.NativePreviewEvent event) {
                if (event.getTypeInt() == Event.ONKEYDOWN) {
                    if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_INSERT) {
                        formHandler.showForm(view);
                    }
                }
            }
        });
    }

}
