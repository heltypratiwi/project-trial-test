/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

/**
 * @author krissadewo
 *         Provide custom search type for window search
 */
public enum ToolbarSearchType {

    SEARCH_TYPE,
    AGAMA,
    BUKU,
    MAHASISWA,
    PEMINJAMAN,
    ASURANSI,
    BANGSAL,
    BIAYA_JENIS,
    JENIS_VENDOR,
    KOMPONEN_BIAYA,
    KOMPONEN_BIAYA_PRODUK,
    PEKERJAAN,
    PENDIDIKAN,
    CARA_BAYAR,
    CARA_MASUK,
    DIAGNOSA,
    DOKTER,
    DOKTER_JADWAL,
    PROPINSI,
    KABUPATEN,
    KECAMATAN,
    KELAS,
    KELURAHAN,
    KUNJUNGAN,
    SPESIALISASI,
    SUB_SPESIALISASI,
    OBAT,
    GOLONGAN_OBAT,
    JENIS_OBAT,
    KEPEMILIKAN_OBAT,
    SATUAN_OBAT_BESAR,
    SATUAN_OBAT_KECIL,
    SEDIAAN_OBAT,
    TERAPI_OBAT,
    POLIKLINIK,
    PRODUK,
    PRODUK_GOLONGAN,
    KAMAR,
    MASTER,
    RUJUKAN,
    PASIEN,
    PASIEN_JENIS,
    TINDAKAN,
    UNIT,
    VENDOR,
    PENERIMAAN_OBAT,
    PENGELUARAN_OBAT,
    HARGA_PRODUK,
    HARGA_OBAT,
    RAWAT_JALAN,
    RAWAT_INAP,
    UGD,
    OPERASI,
}
