/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.menu.dashboard;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.web.client.view.View;
import com.greenlabs.trialproject.web.client.view.custom.GridView;
import com.greenlabs.trialproject.web.client.view.custom.WindowSearchView;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;

/**
 * @author krissadewo <dailycode.org>
 */
public class DashboardView extends View {

    private FlowPanel flowPanel;
    private WindowSearchView windowSearchView;

    @Override
    public Widget asWidget() {
        flowPanel = new FlowPanel();


        GridView gridView = new GridView();
        gridView.setHeaderVisible(false);

        gridView.addWidget(flowPanel, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        return gridView;
    }
}
