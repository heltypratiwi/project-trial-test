/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.menu.settings;

import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.safecss.shared.SafeStyles;
import com.google.gwt.safecss.shared.SafeStylesUtils;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.web.client.icon.Icon;
import com.greenlabs.trialproject.web.client.view.View;
import com.greenlabs.trialproject.web.client.view.custom.AbstractComboBoxRole;
import com.greenlabs.trialproject.web.client.view.custom.GridView;
import com.greenlabs.trialproject.web.client.view.custom.TextButtonSave;
import com.greenlabs.trialproject.web.client.wrapper.CallbackWrapper;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Menu;
import com.greenlabs.trialproject.core.entity.Role;
import com.greenlabs.trialproject.core.entity.RoleMenu;
import com.sencha.gxt.cell.core.client.form.CheckBoxCell;
import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.FocusEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.HeaderGroupConfig;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;
import com.sencha.gxt.widget.core.client.treegrid.TreeGrid;

import java.util.ArrayList;
import java.util.List;

/**
 * @author krissadewo
 */
public class HakAksesView extends View {

    private TreeGrid<Menu> treeMenu;
    private GridView gridView;
    private ComboBox<Role> comboBoxRole;
    private TreeStore<Menu> treeStore;
    private List<Menu> menus = new ArrayList<>();

    @Override
    public Widget asWidget() {
        gridView = new GridView();
        ToolBar toolBar = new ToolBar();
        toolBar.setPack(BoxLayoutContainer.BoxLayoutPack.END);
        toolBar.setSpacing(5);

        Label labelRoleHeader = new Label("ROLE : ");
        labelRoleHeader.addStyleName("label-8");
        toolBar.add(labelRoleHeader);

        comboBoxRole = (ComboBox<Role>) new AbstractComboBoxRole() {
            @Override
            public void loadRelatedMenu() {
                loadMenu(comboBoxRole.getText());
            }
        }.asWidget();
        comboBoxRole.addSelectionHandler(comboBoxRoleSelectionHandler());

        toolBar.add(comboBoxRole);
        toolBar.add(new SeparatorToolItem());

        TextButton textButtonSave = new TextButtonSave();
        textButtonSave.addSelectHandler(buttonSaveSelectHandler());
        toolBar.add(textButtonSave);

        gridView.addWidget(toolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addFocusHandler(new FocusEvent.FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                if (comboBoxRole.getCurrentValue() != null) {
                    loadMenu(comboBoxRole.getCurrentValue().getNama());
                }
            }
        });

        return gridView;
    }

    private boolean isLeafMenu(Menu menu) {
        for (Menu child : menus) {
            if (menu.getId().equals(child.getIdParent())) {
                return false;
            }
        }
        return true;
    }

    private SelectionHandler comboBoxRoleSelectionHandler() {
        return new SelectionHandler() {
            @Override
            public void onSelection(SelectionEvent event) {
                loadMenu(comboBoxRole.getCurrentValue().getNama());
            }
        };
    }

    private void processChildMenu(TreeStore<Menu> treeStore, Menu menu, List<Menu> menus) {
        for (Menu parent : menus) {
            if (parent.getId().equals(menu.getIdParent())) {
                treeStore.add(parent, menu);
            }
        }
    }

    private void loadMenu(final String headerRole) {
        if (treeMenu != null) {
            treeMenu.removeFromParent();
        }

        new CallbackWrapper<ArrayList<Menu>>() {
            @Override
            protected void onSuccess(ArrayList<Menu> result) {
                menus = result;

                ColumnConfig<Menu, String> columnConfigNamaMenu = new ColumnConfig<>(getProperties().getMenuProperties().valueNamaMenu(), 350);
                columnConfigNamaMenu.setHeader("NAMA MENU");

                SafeStyles textStyles = SafeStylesUtils.fromTrustedString("padding: 5% 50% 5% 50%;");

                ColumnConfig<Menu, Boolean> columnConfigCanRead = new ColumnConfig<>(getProperties().getMenuProperties().valueCanRead(), 50);
                columnConfigCanRead.setHeader("read");
                columnConfigCanRead.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
                columnConfigCanRead.setColumnTextStyle(textStyles);
                columnConfigCanRead.setCell(new CheckBoxCell() {
                    @Override
                    public void onBrowserEvent(Context context, Element parent, Boolean value, NativeEvent event, ValueUpdater<Boolean> valueUpdater) {
                        if ("change".equals(event.getType())) {
                            Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                            menu.getRoleMenu().setCanRead(!value);
                            treeStore.update(menu);
                        }
                    }
                });

                ColumnConfig<Menu, Boolean> columnConfigCanSave = new ColumnConfig<>(getProperties().getMenuProperties().valueCanSave(), 50);
                columnConfigCanSave.setHeader("save");
                columnConfigCanSave.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
                columnConfigCanSave.setColumnTextStyle(textStyles);
                columnConfigCanSave.setCell(new CheckBoxCell() {
                    @Override
                    public void onBrowserEvent(Context context, Element parent, Boolean value, NativeEvent event,
                                               ValueUpdater<Boolean> valueUpdater) {
                        if ("change".equals(event.getType())) {
                            Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                            menu.getRoleMenu().setCanSave(!value);
                            treeStore.update(menu);
                        }
                    }

                    @Override
                    public void render(Context context, Boolean value, SafeHtmlBuilder sb) {
                        Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                        if (isLeafMenu(menu)) {
                            super.render(context, value, sb);
                        }
                    }
                });

                ColumnConfig<Menu, Boolean> columnConfigCanEdit = new ColumnConfig<>(getProperties().getMenuProperties().valueCanEdit(), 50);
                columnConfigCanEdit.setHeader("edit");
                columnConfigCanEdit.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
                columnConfigCanEdit.setColumnTextStyle(textStyles);
                columnConfigCanEdit.setCell(new CheckBoxCell() {
                    @Override
                    public void onBrowserEvent(Context context, Element parent, Boolean value, NativeEvent event, ValueUpdater<Boolean> valueUpdater) {
                        if ("change".equals(event.getType())) {
                            Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                            menu.getRoleMenu().setCanEdit(!value);
                            treeStore.update(menu);
                        }
                    }

                    @Override
                    public void render(Context context, Boolean value, SafeHtmlBuilder sb) {
                        Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                        if (isLeafMenu(menu)) {
                            super.render(context, value, sb);
                        }
                    }
                });

                ColumnConfig<Menu, Boolean> columnConfigCanDelete = new ColumnConfig<>(getProperties().getMenuProperties().valueCanDelete(), 50);
                columnConfigCanDelete.setHeader("delete");
                columnConfigCanDelete.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
                columnConfigCanDelete.setColumnTextStyle(textStyles);
                columnConfigCanDelete.setCell(new CheckBoxCell() {
                    @Override
                    public void onBrowserEvent(Context context, Element parent, Boolean value, NativeEvent event, ValueUpdater<Boolean> valueUpdater) {
                        if ("change".equals(event.getType())) {
                            Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                            menu.getRoleMenu().setCanDelete(!value);
                            treeStore.update(menu);
                        }
                    }

                    @Override
                    public void render(Context context, Boolean value, SafeHtmlBuilder sb) {
                        Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                        if (isLeafMenu(menu)) {
                            super.render(context, value, sb);
                        }
                    }
                });


                ColumnConfig<Menu, Boolean> columnConfigCanPrint = new ColumnConfig<>(getProperties().getMenuProperties().valueCanPrint(), 50);
                columnConfigCanPrint.setHeader("print");
                columnConfigCanPrint.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
                columnConfigCanPrint.setColumnTextStyle(textStyles);
                columnConfigCanPrint.setCell(new CheckBoxCell() {
                    @Override
                    public void onBrowserEvent(Context context, Element parent, Boolean value, NativeEvent event, ValueUpdater<Boolean> valueUpdater) {
                        if ("change".equals(event.getType())) {
                            Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                            menu.getRoleMenu().setCanPrint(!value);
                            treeStore.update(menu);
                        }
                    }

                    @Override
                    public void render(Context context, Boolean value, SafeHtmlBuilder sb) {
                        Menu menu = treeStore.findModelWithKey(context.getKey().toString());
                        if (isLeafMenu(menu)) {
                            super.render(context, value, sb);
                        }
                    }
                });

                List<ColumnConfig<Menu, ?>> list = new ArrayList<>();
                list.add(columnConfigNamaMenu);
                list.add(columnConfigCanRead);
                list.add(columnConfigCanSave);
                list.add(columnConfigCanEdit);
                list.add(columnConfigCanDelete);
                list.add(columnConfigCanPrint);
                ColumnModel<Menu> columnModel = new ColumnModel<>(list);

                columnModel.addHeaderGroup(0, 0, new HeaderGroupConfig("", 1, 1));
                columnModel.addHeaderGroup(0, 1, new HeaderGroupConfig(headerRole, 1, 5));

                treeStore = new TreeStore<>(getProperties().getMenuProperties().key());
                for (Menu menu : menus) {
                    if (menu.getIdParent() == 0) {
                        treeStore.add(menu);
                    } else {
                        processChildMenu(treeStore, menu, menus);
                    }
                }

                treeMenu = new TreeGrid<>(treeStore, columnModel, columnConfigNamaMenu);
                treeMenu.getStyle().setLeafIcon(Icon.INSTANCE.menuIcon());
                treeMenu.getView().setAutoExpandColumn(columnConfigNamaMenu);
                treeMenu.getView().setStripeRows(true);
                treeMenu.getView().setColumnLines(true);
                treeMenu.getView().setAutoFill(true);
                treeMenu.setAutoExpand(true);
                treeMenu.setHeight(gridView.getElement().getHeight(true) - 100);
                gridView.addWidget(treeMenu, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
            }

            @Override
            protected void onFailure(Throwable throwable) {
                AppClient.showMessageOnFailureException(throwable);
            }

            @Override
            protected void onCall(AsyncCallback<ArrayList<Menu>> callback) {
                getService().getMenuServiceAsync().createTreeMenu(comboBoxRole.getCurrentValue(), callback);
            }
        }.call();
    }

    private SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                final List<RoleMenu> roleMenus = new ArrayList<>();
                for (Menu menu : treeMenu.getStore().getAll()) {
                    menu.setRole(comboBoxRole.getCurrentValue());

                    RoleMenu roleMenu = new RoleMenu();
                    roleMenu.setId(menu.getRoleMenu().getId());
                    roleMenu.setRole(menu.getRole());
                    roleMenu.setMenu(menu);
                    roleMenu.setCanEdit(menu.getRoleMenu().isCanEdit());
                    roleMenu.setCanRead(menu.getRoleMenu().isCanRead());
                    roleMenu.setCanSave(menu.getRoleMenu().isCanSave());
                    roleMenu.setCanDelete(menu.getRoleMenu().isCanDelete());
                    roleMenu.setCanPrint(menu.getRoleMenu().isCanPrint());
                    roleMenus.add(roleMenu);
                }

                new CallbackWrapper<Result>() {

                    @Override
                    protected void onSuccess(Result result) {
                        AppClient.showInfoMessage(result.getMessage());
                        loadMenu(comboBoxRole.getCurrentValue().getNama());
                    }

                    @Override
                    protected void onFailure(Throwable throwable) {
                        AppClient.showMessageOnFailureException(throwable);
                    }

                    @Override
                    protected void onCall(AsyncCallback<Result> callback) {
                        getService().getRoleMenuServiceAsync().save(roleMenus, callback);
                    }
                }.call();
            }
        };
    }
}
