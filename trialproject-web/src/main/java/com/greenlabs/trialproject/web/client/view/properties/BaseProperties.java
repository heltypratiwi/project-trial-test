/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.properties;

import com.google.gwt.core.client.GWT;

/**
 * @author krissadewo
 *         <p/>
 *         Base class to handle properties for view, register all properties here to
 *         serve our view
 */
public class BaseProperties {

    private static BaseProperties instance;
    private final MenuProperties menuProperties = GWT.create(MenuProperties.class);
    private final MonthProperties monthProperties = GWT.create(MonthProperties.class);
    private final RoleProperties roleProperties = GWT.create(RoleProperties.class);
    private final UserProperties userProperties = GWT.create(UserProperties.class);
    private final CustomFieldProperties customFieldProperties = GWT.create(CustomFieldProperties.class);
    private final AgamaProperties agamaProperties = GWT.create(AgamaProperties.class);
    private final BukuProperties bukuProperties = GWT.create(BukuProperties.class);
    private final MahasiswaProperties mahasiswaProperties = GWT.create(MahasiswaProperties.class);

    private BaseProperties() {
    }

    public static BaseProperties getInstance() {
        if (instance == null) {
            instance = new BaseProperties();
        }
        return instance;
    }

    public CustomFieldProperties getCustomFieldProperties() {
        return customFieldProperties;
    }

    public MenuProperties getMenuProperties() {
        return menuProperties;
    }

    public MonthProperties getMonthProperties() {
        return monthProperties;
    }

    public RoleProperties getRoleProperties() {
        return roleProperties;
    }

    public UserProperties getUserProperties() {
        return userProperties;
    }

    public AgamaProperties getAgamaProperties() {
        return agamaProperties;
    }

    public BukuProperties getBukuProperties() {
        return bukuProperties;
    }

    public MahasiswaProperties getMahasiswaProperties() {
        return mahasiswaProperties;
    }

}
