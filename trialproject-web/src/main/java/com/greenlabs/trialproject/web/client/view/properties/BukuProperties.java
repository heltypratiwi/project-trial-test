package com.greenlabs.trialproject.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.service.BukuService;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

import java.util.Date;

/**
 * Created by Tyas on 11/21/2014.
 */
public interface BukuProperties extends PropertyAccess<Buku> {

    @Editor.Path("Id")
    ModelKeyProvider<Buku> key();

    @Editor.Path("Kode")
    LabelProvider<Buku> labelKode();

    @Editor.Path("Kode")
    ValueProvider<Buku, String> valueKode();

    @Editor.Path("Judul")
    LabelProvider<Buku> labelJudul();

    @Editor.Path("Judul")
    ValueProvider<Buku, String> valueJudul();

    @Editor.Path("Isbn")
    LabelProvider<Buku> labelIsbn();

    @Editor.Path("Isbn")
    ValueProvider<Buku, String> valueIsbn();

    @Editor.Path("createdTime")
    ValueProvider<Buku, Date> valueCreatedTime();

    @Editor.Path("createdBy.realname")
    ValueProvider<Buku, String> valueCreatedBy();
}
