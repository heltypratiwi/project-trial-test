/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.greenlabs.trialproject.core.entity.Role;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * @author krissadewo
 */
public interface RoleProperties extends PropertyAccess<Role> {

    @Editor.Path("id")
    ModelKeyProvider<Role> key();

    @Editor.Path("nama")
    ModelKeyProvider<Role> valueNama();

    @Editor.Path("nama")
    LabelProvider<Role> labelNama();
}
