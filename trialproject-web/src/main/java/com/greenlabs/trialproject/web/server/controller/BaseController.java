/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.server.controller;

import com.greenlabs.trialproject.core.common.Result;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

/**
 * @author krissadewo
 */
public class BaseController {

    /**
     * Converting entity object to json value with specific format
     *
     * @param data
     * @param result
     * @return
     */
    public Map convertModel(Object data, Result result) {
        Map model = new HashMap();
        model.put("data", data);
        model.put("status", result);
        return model;
    }

    /**
     *
     * @param data
     * @param status
     * @return
     */
    public Map<String, Object> convertModel(Object data, Object status) {
        Map<String, Object> model = new HashMap<>();
        model.put("data", data);
        model.put("status", status);
        return model;
    }
}
