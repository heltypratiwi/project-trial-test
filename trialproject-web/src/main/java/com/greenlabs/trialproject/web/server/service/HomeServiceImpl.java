package com.greenlabs.trialproject.web.server.service;

import com.greenlabs.trialproject.web.client.service.GwtHomeService;
import com.greenlabs.trialproject.core.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by krissadewo on 3/10/14.
 */
@Service("gwtHomeService")
public class HomeServiceImpl implements GwtHomeService {

    @Autowired(required = false)
    private Profile profile;

    @Override
    public String getAppTitle() {
        return profile.getAppTitle();
    }
}
