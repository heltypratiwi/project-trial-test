/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.server.service;

import com.greenlabs.trialproject.web.client.service.GwtMenuService;
import com.greenlabs.trialproject.core.AppCore;
import com.greenlabs.trialproject.core.entity.Menu;
import com.greenlabs.trialproject.core.entity.Role;
import com.greenlabs.trialproject.core.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
@Service("gwtMenuService")
public class MenuServiceImpl implements GwtMenuService {

    @Autowired
    private MenuService service;

    @Override
    public ArrayList<Menu> createTreeMenu(Role role) {
        return new ArrayList<>(service.createTreeMenu(role));
    }

    @Override
    public ArrayList<Menu> createTreeMenu() {
        return new ArrayList<>(service.createTreeMenu(AppCore.getInstance().getUserFromSession()));
    }

}
