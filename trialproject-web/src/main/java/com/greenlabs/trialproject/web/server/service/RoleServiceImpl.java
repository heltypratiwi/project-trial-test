/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.server.service;

import com.greenlabs.trialproject.web.client.service.GwtRoleService;
import com.greenlabs.trialproject.core.entity.Role;
import com.greenlabs.trialproject.core.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 25, 2013
 */
@Service("gwtRoleService")
public class RoleServiceImpl implements GwtRoleService {

    @Autowired
    private RoleService roleService;

    @Override
    public ArrayList<Role> findAll() {
        return new ArrayList<>(roleService.findAll());
    }
}
