/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.server.service;

import com.greenlabs.trialproject.web.client.service.GwtRoleUserService;
import com.greenlabs.trialproject.core.entity.RoleUser;
import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.core.service.RoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
@Service("gwtRoleUserService")
public class RoleUserServiceImpl implements GwtRoleUserService {

    @Autowired
    private RoleUserService service;

    public ArrayList<RoleUser> find(User user) {
        return new ArrayList<>(service.find(user));
    }
}
